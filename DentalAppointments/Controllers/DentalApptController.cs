﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DentalAppointments.Models;
using DentalAppointments.ExtensionMethods;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Newtonsoft.Json;

namespace DentalAppointments.Controllers
{ 
    public class DentalApptController : Controller
    {
        

        public IActionResult Index()
        {
            return View(DentalAppointmentLst);
        }

        [HttpGet]
        public IActionResult Create()
        {
            DateTime startTime = DateTime.Now;
            startTime = startTime.AddSeconds(-startTime.Second);

            var newDentalAppointment = new DentalAppointmentModel
            {
                StartTime = DateTime.Now.TruncateToMinute(),
                EndTime = DateTime.Now.TruncateToMinute() 
            }; 

            return View("Create", newDentalAppointment);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(DentalAppointmentModel newDentalAppointment)
        {
            if (ModelState.IsValid)
            {
                var errorFlag = false;
                var errorMsg = string.Empty;
                var doesAppointmentOverlap = DoesAppointmentOverlap(newDentalAppointment);
                TimeSpan duration = newDentalAppointment.EndTime - newDentalAppointment.StartTime;

                if (doesAppointmentOverlap)
                {
                    errorFlag = true;
                    errorMsg += " (*) Appointment with the dentist for that start time exists. ";
                }

                if (newDentalAppointment.StartTime <= DateTime.Now)
                {
                    errorFlag = true;
                    errorMsg += " (*) Appointment must be in the future. ";
                }

                if (duration.TotalMinutes < 30)
                {
                    errorFlag = true;
                    errorMsg += " (*) Appointment must be at least 30 minutes. ";
                }

                TempData["errormessage"] = errorMsg;

                if (!errorFlag)
                {
                    AddDentalAppointment(newDentalAppointment);

                    TempData["Message"] = "Added New Appointment";
                    return RedirectToAction("Index");
                }
            } 
            return View("Create", newDentalAppointment);
        }

        private List<DentalAppointmentModel> _dentalAppointmentLst = null;
        private const string DentalAppointmentLstObgName = "DentalAppointmentLst";

        private bool DoesAppointmentOverlap(DentalAppointmentModel newDentalAppointment)
        {
            var dentalAppt = DentalAppointmentLst.Find(x => x.DentistName == newDentalAppointment.DentistName
                && x.StartTime == newDentalAppointment.StartTime);

            return (dentalAppt != null);
        }

        private void AddDentalAppointment(DentalAppointmentModel newDentalAppointment)
        {
            DentalAppointmentLst.Add(newDentalAppointment);

            TempData.Put(DentalAppointmentLstObgName, DentalAppointmentLst);
             
        }

        private List<DentalAppointmentModel> DentalAppointmentLst
        {
            get
            {
                if (_dentalAppointmentLst == null)
                {
                    if (TempData[DentalAppointmentLstObgName] != null)
                    {
                        _dentalAppointmentLst = TempData.Get<List<DentalAppointmentModel>>(DentalAppointmentLstObgName);

                        TempData.Put(DentalAppointmentLstObgName, _dentalAppointmentLst);
                    }
                    else
                    {
                        _dentalAppointmentLst = new List<DentalAppointmentModel>();
                    }
                }
                return _dentalAppointmentLst;
            }
        }
    }
}