﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DentalAppointments.Models
{
    [Serializable]
    public class DentalAppointmentModel
    {
        [Display(Name = "Start Time")]
        public DateTime StartTime { get; set; }

        [Display(Name = "End Time")]
        public DateTime EndTime { get; set; }

        [Required]
        [Display(Name = "Dentist Name")]
        public string DentistName { get; set; }

        [Required]
        [Display(Name = "Patient Name")]
        public string PatientName { get; set; }
 
    }
}
